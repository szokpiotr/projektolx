package pl.szok.sprzedaj.to.repository;

import org.springframework.data.repository.CrudRepository;

import pl.szok.sprzedaj.to.entity.CategoryOffer;

public interface CategoryRepository extends CrudRepository<CategoryOffer, Long> {

}
