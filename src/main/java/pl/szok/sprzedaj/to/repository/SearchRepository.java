package pl.szok.sprzedaj.to.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pl.szok.sprzedaj.to.entity.Offers;

public interface SearchRepository extends JpaRepository<Offers, Long> {
	@Query("select o from Offers o where o.name like CONCAT(:name,'%')")
	List<Offers> findByName(@Param("name") String name);
}
