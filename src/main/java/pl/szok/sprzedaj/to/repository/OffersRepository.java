package pl.szok.sprzedaj.to.repository;

import org.springframework.data.repository.CrudRepository;

import pl.szok.sprzedaj.to.entity.Offers;

public interface OffersRepository extends CrudRepository<Offers, Long> {

}
