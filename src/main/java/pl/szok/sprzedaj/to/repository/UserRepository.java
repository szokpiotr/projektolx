package pl.szok.sprzedaj.to.repository;

import org.springframework.data.repository.CrudRepository;

import pl.szok.sprzedaj.to.entity.Users;


public interface UserRepository extends CrudRepository<Users, Long> {

}
