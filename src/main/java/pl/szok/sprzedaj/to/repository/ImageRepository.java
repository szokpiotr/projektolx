package pl.szok.sprzedaj.to.repository;

import org.springframework.data.repository.CrudRepository;

import pl.szok.sprzedaj.to.entity.Image;

public interface ImageRepository extends CrudRepository<Image, Long> {

}
