package pl.szok.sprzedaj.to.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szok.sprzedaj.to.entity.Image;
import pl.szok.sprzedaj.to.repository.ImageRepository;
import pl.szok.sprzedaj.to.repository.OffersRepository;

@Controller
@RequestMapping("/image")
public class ImageController {
	
	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	OffersRepository offersRepository;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(ModelMap model){
		model.addAttribute("offers", offersRepository.findAll());
		return "image/add";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String postAdd(@RequestParam("name") String name,
						@RequestParam("url") String url,
						@RequestParam("offerId") long offerId,
						ModelMap model){
		Image image = new Image(name, url, offersRepository.findOne(offerId));
		imageRepository.save(image);
		model.addAttribute("offers", offersRepository.findAll());
		return "image/add";
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") long id,
							ModelMap model){
		Image i = imageRepository.findOne(id);
		i.setOffers(null);
		imageRepository.save(i);
		imageRepository.delete(id);
		model.addAttribute("images", imageRepository.findAll());
		return "image/all";
	}
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String all(ModelMap model){
		model.addAttribute("images", imageRepository.findAll());
		return "image/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") long id,
						ModelMap model){
		model.addAttribute("image", imageRepository.findOne(id));
		return "image/show";
	}
	
}
