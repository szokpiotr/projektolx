package pl.szok.sprzedaj.to.controller;

import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.szok.sprzedaj.to.entity.Offers;
import pl.szok.sprzedaj.to.repository.OffersRepository;

@Controller
public class HomeController {
	
	@Autowired
	OffersRepository offersRepository;
	
	@RequestMapping("/")
	public String homepage(ModelMap model) {
		List<Offers> offers = (List<Offers>) offersRepository.findAll();
		
		if(!offers.isEmpty()){
			Random rand = new Random();
			
			int index = rand.nextInt(offers.size());
		    Offers randomOffer = offers.get(index);
			
			model.addAttribute("offer", randomOffer);
		}
	
		return "index";
	}

	
}
