package pl.szok.sprzedaj.to.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szok.sprzedaj.to.entity.CategoryOffer;
import pl.szok.sprzedaj.to.entity.Offers;
import pl.szok.sprzedaj.to.repository.CategoryRepository;
import pl.szok.sprzedaj.to.repository.OffersRepository;

@Controller
@RequestMapping("/category")
public class CategoryController {
	
	@Autowired
	OffersRepository offersRepository;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String all(ModelMap model){
		model.addAttribute("categories", categoryRepository.findAll());
		return "category/all";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(){
		return "category/addEdit";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String postAdd(@RequestParam("name") String name,
							ModelMap model){
		CategoryOffer category = new CategoryOffer(name);
		categoryRepository.save(category);
		model.addAttribute("categories", categoryRepository.findAll());
		return "category/all";
	}
	
	@RequestMapping(value= "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") long id, 
						ModelMap model) {
		categoryRepository.delete(id);
		model.addAttribute("categories", categoryRepository.findAll());
		categoryRepository.findAll();
		return "category/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") long id,
						ModelMap model){
		model.addAttribute("categoryOffers", categoryRepository.findOne(id).getOffers());
		return "category/show";
	}

}
