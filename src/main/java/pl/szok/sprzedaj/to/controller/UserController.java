package pl.szok.sprzedaj.to.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szok.sprzedaj.to.entity.Users;
import pl.szok.sprzedaj.to.repository.UserRepository;


@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String showRegisterUser() {
		return "user/register";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String RegisterUser(@RequestParam("login") String login,
							   @RequestParam("password") String password,
							   ModelMap model) {
		
		Users user = new Users(login, password);
		userRepository.save(user);
		model.addAttribute("registrationCompleted", "Registration " + user.getLogin().toUpperCase() + " completed!");
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showLogIn() {
		return "user/login";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String logUserIn(@RequestParam("login") String login,
						    @RequestParam("password") String password,
						    ModelMap model,
						    HttpServletRequest request) {
		
		request.getSession().setAttribute("loggedIn", false);
		List<Users> listOfUsers = (List<Users>) userRepository.findAll();
		for(Users u : listOfUsers) {
			if(u.getLogin().equals(login) && u.getPassword().equals(password)) {
				request.getSession().setAttribute("loggedIn", true);
				request.getSession().setAttribute("usersId", u.getId());
				request.getSession().setAttribute("usersLogin", u.getLogin());
				model.addAttribute("loggingInCompleted", "User " + u.getLogin().toUpperCase() + " is logged in!");
				return "index";
			}
		}
		model.addAttribute("failedToLogin", "Bad user's name or password!");
		return "user/login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logUserOut(ModelMap model,
						     HttpServletRequest request) {
		
		Users user = userRepository.findOne((Long) request.getSession().getAttribute("usersId"));
		if(user != null && !user.equals(null)) {
			model.addAttribute("loggedOutUser", user.getLogin());
		}
		request.getSession().setAttribute("loggedIn", false);
		request.getSession().setAttribute("user", null);
		model.addAttribute("loggingOutCompleted", "Logging user out completed!");
		return "index";
	}
	
}
