package pl.szok.sprzedaj.to.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szok.sprzedaj.to.repository.SearchRepository;

@Controller
@RequestMapping("/search")
public class SearchController {

	@Autowired
	private SearchRepository searchRepository;

	@RequestMapping(value = "/byName", method = RequestMethod.GET)
	public String showAll(@RequestParam(value = "name", required = false) String name, Model model) {
		model.addAttribute("search", searchRepository.findByName(name));
		return "search/show";
	}

}
