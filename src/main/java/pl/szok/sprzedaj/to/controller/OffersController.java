package pl.szok.sprzedaj.to.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import pl.szok.sprzedaj.to.entity.CategoryOffer;
import pl.szok.sprzedaj.to.entity.Offers;
import pl.szok.sprzedaj.to.repository.CategoryRepository;
import pl.szok.sprzedaj.to.repository.OffersRepository;
import pl.szok.sprzedaj.to.repository.UserRepository;

@Controller
@RequestMapping("/offers")
public class OffersController {

	@Autowired
	OffersRepository offersRepository;
	
	@Autowired
	CategoryRepository categoryRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
	public String all(ModelMap model) {
		model.addAttribute("offers", offersRepository.findAll());
		return "offers/all";
	}
	
	@RequestMapping(value = "/show/{id}", method = RequestMethod.GET)
	public String show(@PathVariable("id") long id,
						ModelMap model){
		model.addAttribute("offer", offersRepository.findOne(id));
		return "offers/show";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String add(ModelMap model){
		model.addAttribute("categories", categoryRepository.findAll());
		return "offers/addEdit";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String postAdd(@RequestParam("name") String name,
						@RequestParam("description") String description,
						@RequestParam("price") String price,
						@RequestParam("categoryId") long categoryId,
						@RequestParam("location") String location,
						ModelMap model){
		Offers offers = new Offers(name, description, price);
		
		offers.setCategory(categoryRepository.findOne(categoryId));
		
		String[] splits = location.split(",");
		offers.setLng(splits[0].substring(1));
		offers.setLtd(splits[1].substring(1, 15));
		
		offersRepository.save(offers);
		
		model.addAttribute("offers", offersRepository.findAll());
		return "offers/all";
	}
	
	@RequestMapping(value= "/edit/{id}", method = RequestMethod.GET)
	public String edit(@PathVariable("id") long id, 
						ModelMap model) {
		Offers offer = offersRepository.findOne(id);
		offer.setLocation("("+offer.getLng()+", "+offer.getLtd()+")");
		
		offersRepository.save(offer);
		
		model.addAttribute("offer", offersRepository.findOne(id));
		model.addAttribute("categories", categoryRepository.findAll());
		return "offers/addEdit";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String postEdit(@PathVariable("id") long id,
					@RequestParam("name") String name,
					@RequestParam("description") String description,
					@RequestParam("price") String price,
					@RequestParam("categoryId") long categoryId,
					@RequestParam("location") String location,
					ModelMap model){
		Offers offer = offersRepository.findOne(id);
		CategoryOffer c = categoryRepository.findOne(categoryId);
		offer.setName(name);
		offer.setDescription(description);
		offer.setPrice(price);
		offer.setCategory(c);
		
		String[] splits = location.split(",");
		offer.setLng(splits[0].substring(1));
		offer.setLtd(splits[1].substring(1, 15));
		
		offersRepository.save(offer);
		
		return "offers/all";
	}
	
	@RequestMapping(value= "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") long id, 
						ModelMap model) {
		Offers offer = offersRepository.findOne(id);
		
		offer.setCategory(null);
				
		offersRepository.save(offer);
		offersRepository.delete(id);
		model.addAttribute("offers", offersRepository.findAll());
		return "offers/all";
	}
	
}
