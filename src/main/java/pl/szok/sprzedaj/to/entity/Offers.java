package pl.szok.sprzedaj.to.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Offers {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String description;
	private String price;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "offers", cascade = CascadeType.ALL)
	private List<Image> images;
	@ManyToOne(cascade = CascadeType.MERGE)
	private CategoryOffer category;
	@ManyToOne(cascade = CascadeType.MERGE)
	private Users users;
	@Transient
	private String location;
	private String lng; //longitude
	private String ltd; //latitude
	
	public Offers(String name, String description, String price) {
		this.name = name;
		this.description = description;
		this.price=price;
	}

	public Offers() {}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public CategoryOffer getCategory() {
		return category;
	}
	public void setCategory(CategoryOffer category) {
		this.category = category;
	}
	public Users getUsers() {
		return users;
	}
	public void setUsers(Users users) {
		this.users = users;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String longitude) {
		this.lng = longitude;
	}
	public String getLtd() {
		return ltd;
	}
	public void setLtd(String latitude) {
		this.ltd = latitude;
	}

}
