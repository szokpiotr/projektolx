<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="header.jsp"/>

<div class="row">

<c:choose>
	<c:when test="${offer ne null}">
		<div class="col-md-8">
		<h2><font size="1">Promowane | ${offer.name} ${offer.price} zł</font></h2>
		<p></p>
 		<c:forEach items="${offer.images}" var="o">
                <img src="${o.url}" width="304" height="186">
        </c:forEach>
		</div>		
	</c:when>
</c:choose>
</div>

<c:import url="footer.jsp"/>