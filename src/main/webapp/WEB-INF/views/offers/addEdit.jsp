<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="../header.jsp" />

<form id="ciekawy" action="" method="POST">
	<a href='<c:url value="/category/add" />'>Dodaj kategorię</a>

	<div class="form-group">
		<label for="name">Nazwa</label> <input type="text" name="name"
			placeholder="Nazwa" value="${offer.name}">
	</div>

	<div class="form-group">
		<label for="price">Cena</label> <input type="text" name="price"
			placeholder="Podaj cenę" value="${offer.price}">
	</div>

	<div class="form-group">
		<label for="description">Opis</label> <input type="text"
			name="description" placeholder="Informacje o ofercie"
			value="${offer.description}">
	</div>

	<div class="form-group">
		<label for="category">Kategoria</label> <select name="categoryId">
			<c:forEach items="${categories}" var="c">

				<c:choose>
					<c:when test="${c.id eq offers.category.id}">
						<c:set value="selected" var="isSelected" />
					</c:when>
					<c:otherwise>
						<c:set value="" var="isSelected" />
					</c:otherwise>
				</c:choose>
				<option value="${c.id}" ${isSelected}>${c.name}</option>
			</c:forEach>
		</select>
	</div>


	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBRSMT49EId9yskO9KPGY2RSr167moO6sQ"></script>
	<script>
		var map;
		function initialize() {
			var mapOptions = {
				zoom : 6,
				center : new google.maps.LatLng(53.254304, 19.612145)

			};
			map = new google.maps.Map(document.getElementById('map-canvas'),
					mapOptions);
			marker = new google.maps.Marker({
				position : new google.maps.LatLng(53.254304, 19.612145),
				map : map,
				draggable : true

			});

			google.maps.event.addListener(marker, "drag", function() {
				document.getElementById("location").value = marker.position

			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<div class="form-group">
		<input id="location" type="hidden" name="location"
			placeholder="lokalizacja" value="${offer.location}">
	</div>
	<div id="map-canvas" style="height: 300px; width: 500px"></div>

	<div>
		<button type="submit" class="btn btn-default">Zapisz
			ogloszenie</button>
	</div>
</form>

<c:import url="../footer.jsp" />