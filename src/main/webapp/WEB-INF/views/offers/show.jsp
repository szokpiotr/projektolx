<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:import url="../header.jsp" />
<div class="row">
	<div class="col-md-8">
		<h2>${offer.name}</h2>
		<p>${offer.price}zł</p>
		<p>${offer.description}</p>
		<input id="lng" type="hidden" name="lng" placeholder="lokalizacja"
			value="${offer.lng}"> <input id="ltd" type="hidden"
			name="ltd" placeholder="lokalizacja" value="${offer.ltd}">

		<c:forEach items="${offer.images}" var="o">
			<img src="${o.url}" width="504" height="306">
		</c:forEach>

		<script
			src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBRSMT49EId9yskO9KPGY2RSr167moO6sQ"></script>
		<script>
			var map;
			var lng = document.getElementById('lng').value
			var ltd = document.getElementById('ltd').value
			function initialize() {
				var mapOptions = {
					zoom : 6,
					center : new google.maps.LatLng(lng, ltd)

				};

				map = new google.maps.Map(
						document.getElementById('map-canvas'), mapOptions);
				marker = new google.maps.Marker({
					position : new google.maps.LatLng(lng, ltd),
					map : map

				});

			}
			google.maps.event.addDomListener(window, 'load', initialize);
		</script>
		<div id="map-canvas" style="height: 300px; width: 500px"></div>
	</div>
</div>
<c:import url="../footer.jsp" />