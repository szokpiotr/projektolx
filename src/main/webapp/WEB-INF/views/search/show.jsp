<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="../header.jsp" />

<div class="row">
	<div class="col-md-8">
		<c:if test="${search ne null}">

			<c:forEach items="${search}" var="element">

				<h2><a href='<c:url value="/offers/show/${element.id}" />'>${element.name}</a></h2>
				<p>${element.price} zł</p>
				<c:forEach items="${element.images}" var="s">
					<img src="${s.url}" width="204" height="106">
				</c:forEach>
			</c:forEach>
		</c:if>

	</div>
</div>


<c:import url="../footer.jsp" />