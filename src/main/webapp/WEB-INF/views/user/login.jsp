<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<c:import url="../header.jsp" />

	<c:if test="${failedToLogin ne null}">
		<h3>${failedToLogin}</h3>
		<hr />
	</c:if>
	<form method="POST" action='<c:url value="/user/login" />'>
		<h3>Login</h3>
		<hr />
		login: <input type="text" name="login"/>
		hasło: <input type="password" name="password"/>
		<button type="submit">Log in</button>
		<hr />
	</form>
	<div>Nie masz konta? <a href='<c:url value="/user/register"/>'>Zarejestruj się!</a></div>
	
<c:import url="../footer.jsp" />
		