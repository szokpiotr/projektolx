<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<div class="row">
	<div class="col-md-8">
	
		<c:forEach items="${categoryOffers}" var="offer">
			<p> <c:forEach items="${offer.images}" var="o">
                <img src="${o.url}" width="194" height="106">
        </c:forEach> 
        Cena: ${offer.price} zł  <a href='<c:url value="/offers/show/${offer.id}" />'>${offer.name}</a> 
		 <c:choose>
			<c:when test="${sessionScope.loggedIn}">
					<a href='<c:url value="/offers/edit/${offer.id}" />'>Edytuj</a>
			 <a href='<c:url value="/offers/delete/${offer.id}" />'>Usuń</a>  
			</c:when>
		</c:choose>
        </p>
		</c:forEach>
	
	</div>
</div>
<c:import url="../footer.jsp"/>