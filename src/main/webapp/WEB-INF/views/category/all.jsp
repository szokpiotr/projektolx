<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<table class="table">
	<c:forEach items="${categories}" var="category">
			<p><a href='<c:url value="/category/show/${category.id}" />'>${category.name}</a> 
				 <c:choose>
				<c:when test="${sessionScope.loggedIn}">
				<a href='<c:url value="/category/delete/${category.id}" />'>Usuń</a>
				</c:when>
				</c:choose>
			</p>
	</c:forEach>
</table>
<c:import url="../footer.jsp"/>