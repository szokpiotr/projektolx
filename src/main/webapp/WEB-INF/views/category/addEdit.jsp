<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<form action="" method="POST">
  <div class="form-group">
    <label for="name">Kategorie</label>
    <input type="text" name="name" placeholder="Nazwa Kategorii">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
<c:import url="../footer.jsp"/>