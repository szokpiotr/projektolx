<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<table class="table">
	<c:forEach items="${images}" var="img">
		<tr>
			<td>${img.id}</td>
			<td><a href='<c:url value="/image/show/${img.id}" />'>${img.name}</a></td>
			<td><img src="${img.url}" class="img-circle" alt="${img.name}" width="304" height="236"> <a href='<c:url value="/image/delete/${img.id}" />'>Usuń</a></td>
		</tr>
	</c:forEach>
	</tbody>
</table>
<c:import url="../footer.jsp"/>