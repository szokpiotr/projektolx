<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<div class="row">
	<div class="col-md-8">
		<h2>${image.name}</h2>
		<img src="${image.url}" class="img-circle" alt="${img.name}" width="304" height="236">
	</div>
</div>
<c:import url="../footer.jsp"/>