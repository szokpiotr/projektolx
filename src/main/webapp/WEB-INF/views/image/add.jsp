<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="../header.jsp"/>
<form action="" method="POST">
  <div class="form-group">
  
    <label for="name">Nazwa</label>
    <input type="text" name="name" placeholder="Nazwa">
  </div>
  <div class="form-group">
    <label for="url">Link</label>
    <input type="text" name="url" placeholder="http://">
  </div>

  <div class="form-group">
    <label for="city">Oferta</label>
    	<select name="offerId">
            <c:forEach items="${offers}" var="o">
                <option value="${o.id}">${o.name}</option>
            </c:forEach>
        </select>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>
<c:import url="../footer.jsp"/>